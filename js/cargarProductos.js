
var miLista = [
     {
        Marca:"Coca Cola",
        Precio: 150
    },
    {
        Marca:"Inca Cola",
        Precio: 123
    },
    {
        Marca:"Manaos",
        Precio: 95
    },
    {
        Marca:"La bichy",
        Precio: 76
    },
    {
        Marca:"Seven Up",
        Precio: 134
    }
]

window.onload = cargarEventos;

function cargarEventos(){
    document.getElementById("mostrarTabla").addEventListener("click", mostrarTabla, false)
    document.getElementById("cargaBebida").addEventListener("submit", nuevaBebida, false);
}

function mostrarTabla(){
   let cuerpoTabla = document.getElementById("marcasTabla");
   let tablaLlena = "";
    for(var cantidad = 0; cantidad < miLista.length; cantidad++) {
        tablaLlena += "<tr><td>"+miLista[cantidad].Marca+"</td><td>"+miLista[cantidad].Precio+"</td></tr>";
    }
    
    cuerpoTabla.innerHTML = tablaLlena;
}

function nuevaBebida(event){
    event.preventDefault();
    let bebidaIntroducidaPorInput = document.getElementById("bebida").value;
    let precioIntroducidaPorInput = document.getElementById("precio").value;
    let nuevaBebida={
        Marca: bebidaIntroducidaPorInput,
        Precio:precioIntroducidaPorInput
    }
    miLista.push(nuevaBebida);
}